class Trial:
    def __init__(self, odor, trial, id):
        self._odor = odor
        self._trial = trial
        self._id = id

    def get_odor(self):
        return self._odor

    def get_trial(self):
        return self._trial

    def get_id(self):
        return self._id
