class Filter:
    def __init__(self, odor, roi, filt):
        self._odor = odor
        self._roi = roi
        self._filter = filt

    def get_odor(self):
        return self._odor

    def get_roi(self):
        return self._roi

    def get_filter(self):
        return self._filter
