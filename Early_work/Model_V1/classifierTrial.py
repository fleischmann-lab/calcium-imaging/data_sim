from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LogisticRegression
from pynwb import NWBHDF5IO
from scipy import signal, interp
from scipy.stats.distributions import chi2
from sklearn import metrics, random_projection
import matplotlib.pyplot as plt
import seaborn as sns
from glmTry import *
from sklearn.decomposition import PCA
import csv
from sklearn.decomposition import TruncatedSVD
from scipy.sparse import random as sparse_random

"""
            CURRENT IMPLEMENTATION SET UP FOR RECORDED + SIMULATED SPIKES
            THIS VERSION SHOWED HIGHEST ACCURACY MEASUREMENTS
            CAN EASILY BE ADAPTED TO JUST RECORDED? SIMULATED SPIKES OR SPLIT AS TRAINING AND TESTING ON ONE OF THE TWO DATASETS

            NEXT STEPS: Do results hold for inter mouse comparison? Classification performance compared for highest 
            performing simulated data model? Better understanding of dimensionality reduction techniques/overfitting
            Plot figures of dimensionality reduction tools + accuracy for chosen model and chosen dataset
            
"""

""" 315 cells, 8 trials for 10 odors, 10 odors with 2520 trials each, 2520 trials total
For indiv cell in all cells:
    for trial in cell full 10880 response
        at each value (0 or 1) look at 30seconds behind (auto history)
        at each value (0 or 1) look at average 30seconds for all 315 cells
        at each value (0 or 1) look at average 30seconds sniff trace
        for that trial make design matrix (auto (136), cross (136), sniff (136))
2520 trials each of 3,136
so 2520,408 = X, 2520 = Y

If using combined recorded and simulated datasets, will be *2
"""


dt = float(30 / 136)

# Load data
io = NWBHDF5IO(
    "/Users/andrewaoun/Desktop/Fleischmann_Lab/data_sim/data.nosync/testing.nwb", "r"
)
nwbfile_in = io.read()
flow = nwbfile_in.acquisition["flow"].data.value

# Make stimulus pulses
pulses = np.zeros(10880)
for i in range(80):
    pulses[(136 * i + 45) : (136 * i + 50)] = 1

# Get relevant data out of NWB file
wheel = nwbfile_in.acquisition["wheel"].data.value
odor = nwbfile_in.stimulus["odor"].data.value

p1 = (
    nwbfile_in.processing["ophys"]
    .data_interfaces["Deconvolved"]
    .roi_response_series["Plane_1"]
    .data.value
)
p2 = (
    nwbfile_in.processing["ophys"]
    .data_interfaces["Deconvolved"]
    .roi_response_series["Plane_2"]
    .data.value
)
p3 = (
    nwbfile_in.processing["ophys"]
    .data_interfaces["Deconvolved"]
    .roi_response_series["Plane_3"]
    .data.value
)

deconvolved = np.vstack((p1, p2, p3))
spikes = np.zeros((deconvolved.shape))

# Deconvolved spikes
for i in range(deconvolved.shape[0]):
    spikes[
        i,
        np.where(
            deconvolved[i, :]
            > (np.mean(deconvolved[i, :] + 1 * np.std(deconvolved[i, :])))
        ),
    ] = 1

# Load in simulated dataset
filename = "sim_dataset.csv"
sim_spikes = []
with open(filename, "r") as csvfile:
    csvreader = csv.reader(csvfile)
    for row in csvreader:
        sim_spikes.append(row)
sim_spikes = np.array(sim_spikes, dtype=np.float32)
print(sim_spikes)

f1 = (
    nwbfile_in.processing["ophys"]
    .data_interfaces["Fluorescence"]
    .roi_response_series["Plane_1"]
    .data.value
)
f2 = (
    nwbfile_in.processing["ophys"]
    .data_interfaces["Fluorescence"]
    .roi_response_series["Plane_2"]
    .data.value
)
f3 = (
    nwbfile_in.processing["ophys"]
    .data_interfaces["Fluorescence"]
    .roi_response_series["Plane_3"]
    .data.value
)

fluorescence = np.vstack((f1, f2, f3))

# Downsample to 10880
flow = signal.resample(flow, 10880)
print("Flow", len(flow))
wheel[np.isnan(wheel)] = 0
wheel = signal.resample(wheel, 10880)
print("Wheel", len(wheel))

# Set up labels = odors
temp = np.zeros(10880)
for i in range(80):
    temp[i * 136 : (i + 1) * 136] = odor[i * 30000]
odor = temp
print("Odor", len(odor))

""" 315 cells, 8 trials for 10 odors, 10 odors with 2520 trials each, 2520 trials total
For indiv cell in all cells:
    for trial in cell full 10880 response
        at each value (0 or 1) look at 30seconds behind (auto history), 20 filters
        at each value (0 or 1) look at average 30seconds for all 315 cells, 20 filters
        at each value (0 or 1) look at average 30seconds sniff trace
        for that trial make design matrix (auto (136), cross (136), sniff (136))
X shape: 25200,42,136 --> Dimensionality reduction to get 25200, 42 (PCA/SVD/SRP used)
Y shape: 25200
split as 
20200 train
5000 test
"""

# Two runs, one for original spikes one for simulated spikes
for dtype in range(2):
    if dtype == 1:
        spikes = sim_spikes
    dt = float(30 / 136)
    nBasis_auto = 20
    nBasis_cross = 20
    nTaus = 136  # Extent of history window, for us 136 time bins is 30 second window or one full trial backwards
    ht = make_cosine_basis(nBasis_auto, 136, 1, offset=0, normalize=True)
    ht = ht[:, :nTaus]
    ht = np.append(np.zeros((nBasis_auto, 1)), ht, axis=1)  #######################
    T = nTaus
    time = np.arange(0, T + dt, dt)

    # Will build the trials array that will hold 315 cells, 40 filters, 80 trials, 136 length of filter
    X_auto = np.zeros((315, nBasis_auto, 80, 136))
    trials = np.zeros((315, nBasis_cross + nBasis_auto, 80, 136))
    labels = np.zeros(25200)
    flow_trials = np.zeros((25200, 136))
    X0 = np.ones((136, 1))
    for i in range(315):
        for j in range(80):
            labels[i * 80 + j] = odor[j * 136]
            flow_trials[i * 80 + j, :] = flow[j * 136 : (j + 1) * 136]
            for k in range(nBasis_auto):
                X_auto[i, k, j, :] = signal.lfilter(
                    ht[k, :], 1, spikes[i, j * 136 : (j + 1) * 136]
                )
    for i in range(80):
        X_cross = np.zeros((315, nBasis_cross, 136))
        for j in range(nBasis_cross):
            X_cross[:, j, :] = signal.lfilter(
                ht[j, :], 1, spikes[:, i * 136 : (i + 1) * 136]
            )
        cross_mean = np.mean(X_cross, axis=0)
        trials[:, :nBasis_auto, i, :] = X_auto[:, :, i, :]
        trials[:, nBasis_auto : nBasis_auto + nBasis_cross, i, :] = cross_mean[:, :]

    # Reshape as 2520 trials of a 42x136 design matrix
    trials = np.reshape(trials, (int(315 * 80), 40, 136))
    input_data = np.zeros((25200, int(42 * 136)))
    for i in range(25200):
        X = np.hstack((X0, flow_trials[i].reshape(136, 1), trials[i, :, :].T))
        input_data[i] = X.reshape(int(42 * 136))

    # # PCA - DIMENSIONALITY REDUCTION
    # pca = PCA(n_components=42)
    # pcas = []
    # input_data = input_data.reshape(25200, 42, 136)
    # for i in range(25200):
    #     pca.fit(input_data[i,:,:])
    #     pcas.append(pca.singular_values_)

    # # SRP - DIMENSIONALITY REDUCTION
    # pcas = []
    # input_data = input_data.reshape(25200, 42, 136)
    # transformer = random_projection.SparseRandomProjection(n_components=42)
    # # fitted = transformer.fit_transform(input_data[:,:])
    # for i in range(25200):
    #     X_new = transformer.fit_transform(input_data[i,:,:])
    #     pcas.append(X_new)

    # SVD - DIMENSIONALITY REDUCTION
    svd = TruncatedSVD(n_components=42, random_state=42)
    pcas = []
    input_data = input_data.reshape(25200, 42, 136)
    for i in range(25200):
        svd.fit(input_data[i, :, :])
        pcas.append(svd.singular_values_)

    print(np.array(pcas).shape)
    input_data = pcas
    if dtype == 0:
        real_spikes = spikes
        real_pcas = np.array(input_data)
    if dtype == 1:
        sim_spikes = spikes
        sim_pcas = np.array(input_data)

    # idx = np.random.choice(25200,size=23200, replace=False)
    # idx = np.random.choice(25200,size=25200, replace=False)
    # all_idx = np.arange(25200)
    # idx2 = []
    # for i in range(len(all_idx)):
    #     if all_idx[i] not in idx:
    #         idx2.append(all_idx[i])

spikes = real_spikes
input_data = real_pcas

# The following code shuffles the SIMULATED spikes independently of the original
# spikes and splits into train and test
idx1 = np.random.choice(25200, size=20000, replace=False)
all_idx1 = np.arange(25200)
idx2 = []
for i in range(len(all_idx1)):
    if all_idx1[i] not in idx1:
        idx2.append(all_idx1[i])
idx2 = np.array(idx2)

# X_test = sim_pcas[idx,:]
# y_test = labels[idx]

sxTrain = sim_pcas[idx1, :]
syTrain = labels[idx1]
sxTest = sim_pcas[idx2, :]
syTest = labels[idx2]

# The following code shuffles the ORIGINAL spikes independently of the original
# spikes and splits into train and test
idx3 = np.random.choice(25200, size=20000, replace=False)
all_idx2 = np.arange(25200)
idx4 = []
for i in range(len(all_idx2)):
    if all_idx2[i] not in idx3:
        idx4.append(all_idx2[i])
idx4 = np.array(idx4)

rxTrain = sim_pcas[idx3, :]
ryTrain = labels[idx3]
rxTest = sim_pcas[idx4, :]
ryTest = labels[idx4]

# Concat training samples for simulated and original
X = np.vstack((sxTrain, rxTrain))
y = np.hstack((syTrain, ryTrain))
X_test = np.vstack((sxTest, rxTest))
y_test = np.hstack((syTest, ryTest))

# Reshuffle concatenated matrix
size1 = len(idx3)
size2 = 25200 - size1
idx1 = np.random.choice(25200, size=size1, replace=False)
idx2 = np.random.choice(25200, size=size2, replace=False)

X = X[idx1]
y = y[idx1]
X_test = X_test[idx2]
y_test = y_test[idx2]

# idx1 = np.random.choice(25200,size=20000, replace=False)
# all_idx1 = np.arange(25200)
# idx2 = []
# for i in range(len(all_idx1)):
#     if all_idx1[i] not in idx1:
#         idx2.append(all_idx1[i])
# idx2 = np.array(idx2)

# X = sim_pcas[idx1,:]
# y = labels[idx1]

# idx1 = np.random.choice(25200,size=20000, replace=False)
# all_idx1 = np.arange(25200)
# idx2 = []
# for i in range(len(all_idx1)):
#     if all_idx1[i] not in idx1:
#         idx2.append(all_idx1[i])
# idx2 = np.array(idx2)

# X_test  = input_data[idx1,:]
# y_test = labels[idx1]

# X_test = np.array(input_data)[idx2,:]
print("X_test shape", str(X_test.shape))
print("X_train shape", str(X.shape))
# y_test = labels[idx2]
print("y_test shape", str(y_test.shape))
print("y_train shape", str(y.shape))

# for multi_class in ('multinomial', 'ovr', 'auto'):
for multi_class in ["auto"]:
    clf = LogisticRegression(solver="sag", max_iter=10000, multi_class=multi_class).fit(
        X, y
    )

    print(np.array(clf.coef_).shape)
    # print the training scores
    print("training score : %.3f (%s)" % (clf.score(X, y), multi_class))
    # create a mesh to plot in
    h = 1  # step size in the mesh
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    Z = clf.predict(X_test)

    score = clf.score(X_test, y_test)
    print("testing score : " + str(score))
    cm = metrics.confusion_matrix(y_test, Z)
    plt.figure(figsize=(12, 12))
    sns.heatmap(cm, annot=True, fmt=".3f", linewidths=0.5, square=True)
    # cmap = 'Blues_r')
    plt.ylabel("Actual label")
    plt.xlabel("Predicted label")
    all_sample_title = "Accuracy Score: {0}".format(score)
    plt.title(all_sample_title, size=15)

plt.show()
quit()

"""                               """
"""                               """
""" IGNORE CODE BEYOND THIS POINT """
"""                               """
"""                               """


# trials = np.zeros((2520,22,136))
# X0 =  np.ones((136, 1))
# labels = np.zeros(2520)
# for i in range(315):
#     # print(i)
#     X_cross = np.ones((315, nBasis_cross, 10880))
#     X_auto = np.zeros((nBasis_auto, 10880))
#     # temp = np.zeros((315, nBasis_cross, 80, 136))
#     # for k in range(10):
#     #     X_auto[k, :] = signal.lfilter(ht[k,:], 1, spikes[i,:])
#     #     X_cross[:, k, :] = signal.lfilter(ht[k,:], 1, spikes[i,:])
#     # for j in range(80):
#     #     temp[:,:,j*136:(j+1)*136,:] =  X_cross[:, :, (j+1)*136]

#     # X_cross = np.mean(X_cross, axis=0)
#     # Xdsg = np.vstack((X_auto, X_cross)).T
#     # # print(Xdsg.shape)
#     # X = np.hstack((X0, flow_trial.reshape(136,1), Xdsg))
#     # trials[i+j,:,:] = X.T
#     for j in range(80):
#         # print(j)
#         trial = spikes[i,j*136:(j+1)*136]
#         flow_trial = flow[j*136:(j+1)*136]
#         other_trials = spikes[:,j*136:(j+1)*136]
#         for k in range(10):
#             # print(k)
#             X_auto[k, :] = signal.lfilter(ht[k,:], 1, trial)
#         # other_trials = spikes[:,j*136:(j+1)*136]
#         # for k in range(nBasis_cross):
#             X_cross[:, k, :] = signal.lfilter(ht[k,:], 1, other_trials)
#         # for n in range(315):
#         #     if n != i:
#         #         other_trial = spikes[n,j*136:(j+1)*136]
#         #         for k in range(nBasis_cross):
#         #             X_cross[n, k, :] = signal.lfilter(ht[k,:], 1, other_trial)
#         X_cross = np.mean(X_cross, axis=0)
#         Xdsg = np.vstack((X_auto, X_cross)).T
#         # print(Xdsg.shape)
#         X = np.hstack((X0, flow_trial.reshape(136,1), Xdsg))
#         trials[i+j,:,:] = X.T


# Sum spike counts for all trials of each odor in a cell (repeat for 315 cells),
# divide up trials by odor (Odor 1: 315 avg responses, Odor 2: 315 avg responses etc...)
# ensembles = []
# for j in range(315):
#     ensemble = [[] for i in range(10)]
#     for i in range(80):
#         trial = spikes[j,i*136:(i+1)*136]
#         ensemble[int(odor[i*136])-1].append(trial)
#     ensembles.append(ensemble)
# ensembles = np.array(ensembles)
# print(ensembles.shape)
# batch_size = 6
# test_size = 60
# train_size = 252

# idx = np.random.choice(312, size=252, replace=False)
# all_idx = np.arange(312)
# idx2 = []
# for i in all_idx:
#     if i not in idx:
#         idx2.append(i)
# # print(spikes.shape)

# trial_odor = []
# for i in range(10):
#     for j in range(8):
#         trial_odor.append(i)
# trial_odor = np.array(trial_odor)
# ensembles = np.reshape(ensembles, (315,80,136))
# train = ensembles[idx,:,:]
# test = ensembles[idx2,:,:]


# X = train.T
# X_test = X[70*136:80*136]
# X = X[0*136:70*137]

counts_all = np.sum(ensembles, axis=0).reshape((80, 136))

print(counts_all.shape)
print(train.shape)
# sto
# trial_odor = np.zeros((len(train),10))
# for i in range(len(train)):
#     for j in range(len(train[i])):


y = trial_odor
X0 = np.ones((len(y), 1))
# X = np.hstack((flow.reshape(-1, 1, order='F'), odor.reshape(-1, 1, order='F')))
# print(X.shape)
# print(y.shape)

# Code for making auto and cross history filter
nBasis_auto = 10
nBasis_cross = 10
nTaus = 30
ht = make_cosine_basis(nBasis_auto, 33, 10, offset=0, normalize=False)
# print(ht.shape)
ht = ht[:, :30]
# print(ht.shape)
ht = np.append(np.zeros((10, 1)), ht, axis=1)  #######################
# print(ht.shape)
T = 30
time = np.arange(0, T + dt, dt)

nCells = 315
N = len(time) - 1

X_cross = np.ones((315, nBasis_cross, 136))
X_auto = np.zeros((315, nBasis_auto, 136))
for n in range(315):
    # for c in rnage(80):
    for k in range(nBasis_auto):
        X_auto[n, k, :] = signal.lfilter(
            ht[k, :], 1, np.array(ensembles[n, k, :]).flatten()
        )
        X_cross[n, k, :] = signal.lfilter(ht[k, :], 1, counts_all[k, :].flatten())
# X_cross = np.reshape(X_cross, (315,10,8,136))
# X_auto = np.reshape(X_auto, (315,10,8,136))
# X_cross = np.mean(X_cross, axis=2)
# X_auto = np.mean(X_auto, axis=2)

X_cross = np.mean(X_cross, axis=0)
X_auto = np.mean(X_auto, axis=0)

# X_cross = np.mean(X_cross, axis=0).reshape(1,136)
# X_auto = np.mean(X_auto, axis=0).reshape(1,136)
print(X_auto.shape)
print(X_cross.shape)
# Xdsg = np.append(X_auto, X_cross, axis=0).T
Xdsg = np.hstack((X_auto, X_cross)).T

print(Xdsg.shape)
Xdsg = Xdsg.reshape((80, 136 * 2))
print(Xdsg.shape)
# X = np.hstack((X0, flow.reshape((80,136)), wheel.reshape((80,136)), Xdsg))
# X = np.hstack([np.ones((Xdsg.shape[0], 1), dtype=Xdsg.dtype), Xdsg])
X = np.hstack((X0, flow.reshape((80, 136)), Xdsg))
# X = np.hstack((flow.reshape(-1, 1, order='F'), X_auto.T))
plt.plot(X_cross.T)
plt.show()
plt.plot(X_auto.T)
plt.show()
print(X.shape)

indices = np.random.choice(80, size=60, replace=False)
all_idx = np.arange(80)
indices2 = []
for i in all_idx:
    if i not in indices:
        indices2.append(i)


# X_test = X[:,idx2]
# X_test = X
# y_test = y
# y = y
# X = X[:,idx]

X_test = X[indices, :]
y_test = y[indices]
X = X[indices2, :]
y = y[indices2]


for multi_class in ("multinomial", "ovr", "auto"):
    clf = LogisticRegression(solver="sag", max_iter=10000, multi_class=multi_class).fit(
        X, y
    )

    print(np.array(clf.coef_).shape)
    # print the training scores
    print("training score : %.3f (%s)" % (clf.score(X, y), multi_class))
    # create a mesh to plot in
    h = 1  # step size in the mesh
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # h = 1
    # xx, yy = np.meshgrid(np.arange(0, 10880, h),
    #                      np.arange(0, 3, h))

    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, x_max]x[y_min, y_max].
    # print(xx.ravel().shape)
    # print(yy.ravel().shape)
    # print(np.c_[xx.ravel(), yy.ravel()].shape)
    # Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = clf.predict(X_test)
    # Put the result into a color plot
    colors = [
        "blue",
        "yellow",
        "green",
        "red",
        "purple",
        "brown",
        "pink",
        "grey",
        "olive",
        "cyan",
    ]
    # print(Z[0:1000])
    # print(odor[0:1000])
    print(Z.shape)
    # Z = Z.reshape(xx.shape)
    # plt.figure()
    # plt.contourf(xx, yy, Z, cmap=plt.cm.Paired)
    # plt.title("Decision surface of LogisticRegression (%s)" % multi_class)
    # plt.axis('tight')

    # score = logisticRegr.score(x_test, y_test)
    # print(score)

    score = clf.score(X_test, y_test)
    cm = metrics.confusion_matrix(y_test, Z)
    plt.figure(figsize=(9, 9))
    sns.heatmap(cm, annot=True, fmt=".3f", linewidths=0.5, square=True, cmap="Blues_r")
    plt.ylabel("Actual label")
    plt.xlabel("Predicted label")
    all_sample_title = "Accuracy Score: {0}".format(score)
    plt.title(all_sample_title, size=15)

plt.show()
