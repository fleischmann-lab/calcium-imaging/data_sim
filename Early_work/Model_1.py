# THIS VERSION USES AN UNDIRECTED GRAPH TO MODEL MULTIPLE ODOR PAIRS
# WITH VARYING OVERLAP RANGES

import datetime
import time
import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plot
import math as m
from scipy.stats import poisson
from sklearn.preprocessing import normalize
from get_data import Data
from Trial import Trial
from Neuron import Neuron
from scipy.stats import iqr
from OdorNet import Odor, Similarity, OdorNet, Ensemble

# INPUTS
set_size = 100
odor1 = Odor("1")
odor2 = Odor("2")
similarity_1_2 = Similarity(10, 20)
net = OdorNet()
net.addOdor(odor1)
net.addOdor(odor2)
net.addOverlap(odor1, odor2, similarity_1_2)
nmb_odors = len(net.getOdorSet())
odor_set = net.getOdorSet()
activation = 0.1
similarity_set = net.getSimilaritySet()

ensemble_size = int(activation * set_size)

# thresholds = [454.0, 455.0, 458.0, 456.0, 452.0,
# 456.0, 454.0, 454.0, 458.0, 456.0]

# MAKE NEURONS
# 100 neurons, each neuron is an LNP neuron with 10
# odors presented across 10 trials
neuron_set = []
for i in range(set_size):
    neuron = Neuron()
    neuron_set.append(neuron)

# MAKE ENSEMBLES DICT
ensembles = {}
for odor in odor_set:
    ensembles[odor] = []
print(ensembles)

# MAKE ENSEMBLES WITH OVERLAP SPECIFICITY
while len(similarity_set) > 0:
    popped = similarity_set.pop()
    # print(popped.getOdor())
    neurons = []
    neighbors = popped.getOdorPair()
    val_range = popped.getRange()
    percentage = random.randrange(val_range[0], val_range[1], 1)
    neurons_to_add = int(ensemble_size * (percentage / 100))
    for i in range(neurons_to_add):
        if len(neurons) != 10:  # Cannot add more than 100 neurons (10%),
            # this signifies overlap values added add up to more than 100%
            # print("hi")
            neurons.append(neuron_set[random.randint(0, set_size - 1)])
    for odor in neighbors:
        temp = ensembles[odor]
        ensembles[odor] = temp + neurons
# print(ensembles)

# FILL ENSEMBLES UP TO THE 10 NEURON MARK
for odor in ensembles.keys():
    while len(ensembles[odor]) != ensemble_size:
        remaining = int(ensemble_size - len(ensembles[odor]))
        # print(remaining)
        neuron_index = int(random.randint(0, set_size - 1))
        if neuron_index not in ensembles.values():
            ensembles[odor] += [neuron_set[neuron_index]]

sum = 0
for val in ensembles.values():
    for val2 in ensembles.values():
        if val == val2:
            sum += 1

# GEN SPIKES

run = []
for odor in ensembles.keys():
    neurons = ensembles[odor]
    for neuron in neurons:
        trials = neuron.get_trial_list()
        for trial in trials:
            # print(trial.odor())
            # print(odor.getOdor())
            if str(trial.odor()) == str(odor.getOdor()):
                trial.set_activation(True)

for neuron in neuron_set:
    neuron.run_trials()

for odor in ensembles.keys():
    for neuron in ensembles[odor]:
        print(
            "odor: "
            + str(odor.getOdor())
            + " ; neuron: "
            + str(neuron)
            + " ; spikes: "
            + neuron.get_output_spikes()
        )


# # run 1 (10 trials each odor once)
# ensembles_1 = []
# for odor in range(nmb_odors):
#     ensembles_1.append(random.sample(range(0, set_size), int(activation*set_size)))
# # print(ensembles_1)

# # run 2 (10 trials each odor once), 80% neuron overlap,
# 20% different --> T to T variability
# ensembles_2 = []
# for odor in range(nmb_odors):
#     temp = []
#     prev = []
#     for i in range(int(nmb_odors*similarity)):
#         rand = random.randint(0,9)
#         if rand not in prev:
#             temp.append(ensembles_1[odor][rand])
#         prev.append(rand)
#     prev = []
#     ensembles_2.append(temp)
# c = nmb_odors-int(nmb_odors*similarity)
# for odor in range(nmb_odors):
#     while c > 0:
#         to_append = random.sample(range(0, set_size), c)
#         for val in to_append:
#             if val not in ensembles_2 and c != 0:
#                 c -= 1
#                 ensembles_2[odor].append(val)
# # print(ensembles_2)

# # RUN NMB 1
# run_1 = []
# odor = 1
# for ensemble in ensembles_1:
#     ens = []
#     for neuron in ensemble:
#         trials = neuron_set[neuron].get_trial_list()
#         for trial in trials:
#             if trial.odor() == odor:
#                 trial.set_activation(True)
#         ens.append(neuron_set[neuron].run_trials())
#     odor += 1
#     run_1.append(ens)
# print(len(run_1[0]))

# # RUN NMB 1
# run_2 = []
# odor = 1
# for ensemble in ensembles_2:
#     ens = []
#     for neuron in ensemble:
#         trials = neuron_set[neuron].get_trial_list()
#         for trial in trials:
#             if trial.odor() == odor:
#                 trial.set_activation(True)
#         ens.append(neuron_set[neuron].run_trials())
#     odor += 1
#     run_2.append(ens)
# print(len(run_2))


# set_size = 2000
# Make 100 neurons
# neuron_set = []
# for i in range(0, set_size):
#     neuron = Neuron()
#     temp = []
#     for trial in neuron.get_trial_list():
#         temp = neuron.run_trials()
#     neuron_set.append(temp)
# print(neuron_set)


# Find Q3, Q1, IQR
# stats = []
# for i in range(10):
#     temp = []
#     for neuron in neuron_set:
#         temp.append(neuron[i])
#     stats.append(np.percentile(temp, [25, 50, 75]))
# print(stats)

# threshold = []
# for i in range(len(stats)):
#     threshold.append(stats[i][2])
# print("hi")
# print(threshold)

# Find average spikes for each odor
# avg = []
# for i in range(10):
#     sum = 0
#     for neuron in neuron_set:
#         if neuron[i] > stats[i][0] - 1.5 * (stats[i][2]-stats[i][0]) and neuron[i]
# < stats[i][2] + 1.5 * (stats[i][2]-stats[i][0]):
#             sum += neuron[i]
#     avg.append(sum/set_size)
# print(avg)
