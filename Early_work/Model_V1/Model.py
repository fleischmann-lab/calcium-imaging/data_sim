# THIS VERSION MODELS A SINGLE OVERLAP RANGE WITH A SIGNLE TRIAL TO TRIAL VARIABILITY FOR MULTIPLE ODOR PAIRS

import datetime
import time
import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plot
import math as m
from scipy.stats import poisson
from sklearn.preprocessing import normalize
from get_data import Data
from Trial import Trial
from Neuron import Neuron
from scipy.stats import iqr
from OdorNet import Odor, Similarity, OdorNet, Ensemble


saturation = 0.5
set_size = 1000
activation = 0.1
overlap = 0.2
trial_to_trial = 0.3
trial_count = 5

odor1 = Odor("1")
odor2 = Odor("2")
# odor3 = Odor('3')
# odor4 = Odor('4')
# odor5 = Odor('5')
net = OdorNet()
net.addOdor(odor1)
net.addOdor(odor2)
# net.addOdor(odor3)
# net.addOdor(odor4)
# net.addOdor(odor5)
odor_set = net.getOdorSet()
ensemble_size = int(activation * set_size)

# MAKE NEURONS
# 1000 neurons, each neuron is an LNP neuron with 10 odors presented across 10 trials
neuron_set = []
for i in range(set_size):
    neuron = Neuron()
    neuron_set.append(neuron)
    neuron.set_id(i + 1)

# MAKE ENSEMBLES DICT
# ensembles = {}
# for odor in odor_set:
#     ensembles[odor] = []

# MAKE ENSEMBLES AND TRIALS DICT
trials = []
while len(trials) < trial_count:
    ensembles = {}
    for odor in odor_set:
        ensembles[odor] = []
    trials.append(ensembles)

# GETS ALL NEURONS ALREADY TAKEN FROM NEURON_SET
# def get_all_populated_neurons():
#     temp = set()
#     for ensembles in trials:
#         for odor in odor_set:
#             temp.add(ensembles[odor])
#     return temp

# keep track of neurons that we are using to maintain trial-trial and overlap similarity
all_populated_neurons = set()

# POPULATE EACH ENSEMBLE WITH THE OVERLAP NEURONS
overlap_neurons = []
c = int(overlap * ensemble_size)
while c > 0:
    # print(int(overlap*ensemble_size))
    # temp = get_all_populated_neurons()
    to_append = neuron_set[random.randint(0, set_size - 1)]
    if to_append not in all_populated_neurons and to_append not in overlap_neurons:
        c -= 1
        overlap_neurons.append(to_append)
for odor in odor_set:
    for ensembles in trials:
        ensembles[odor] += overlap_neurons
for neuron in overlap_neurons:
    all_populated_neurons.add(neuron)

# POPULATE EACH ENSEMBLE WITH THE NON-VARIABLE NEURONS
for odor in odor_set:
    non_variable_neurons = []
    c = int(saturation * ensemble_size)
    while c > 0:
        to_append = neuron_set[random.randint(0, set_size - 1)]
        # temp = get_all_populated_neurons()
        # for val in ensembles.values():
        #     temp += val
        if (
            to_append not in all_populated_neurons
            and to_append not in non_variable_neurons
        ):
            c -= 1
            non_variable_neurons.append(to_append)
    for ensembles in trials:
        ensembles[odor] += non_variable_neurons

    for neuron in non_variable_neurons:
        all_populated_neurons.add(neuron)

# POPULATE EACH ENSEMBLES WITH THE VARIABLE NEURONS
for ensembles in trials:
    for odor in odor_set:
        variable_neurons = []
        c = int(trial_to_trial * ensemble_size)
        while c > 0:
            to_append = neuron_set[random.randint(0, set_size - 1)]
            # temp = get_all_populated_neurons()
            # print(len(temp))
            # for val in ensembles.values():
            #     temp += val
            if (
                to_append not in all_populated_neurons
                and to_append not in variable_neurons
            ):
                # print("hi")
                # print(to_append in temp)
                # print(to_append in variable_neurons)
                c -= 1
                variable_neurons.append(to_append)
        ensembles[odor] += variable_neurons
        for neuron in variable_neurons:
            all_populated_neurons.add(neuron)

temp = []
for ensembles in trials:
    for odor in odor_set:
        for val in ensembles[odor]:
            if val not in temp:
                temp.append(val)
print(len(temp))


# GENERATE SPIKES
for odor in odor_set:
    for ensembles in trials:
        for neuron in ensembles[odor]:
            trial = neuron.get_trial_list()
            for t in trial:
                if str(t.odor()) == str(odor.getOdor()):
                    t.set_activation(True)
for neuron in neuron_set:
    neuron.run_trials()

# PRINT OUT ENSEMBLES
trial_counter = 1
for ensembles in trials:
    print("run ", trial_counter, "\n")
    for odor in odor_set:
        print("    new odor ensemble \n")
        for neuron in ensembles[odor]:
            print(
                "        odor: "
                + str(odor.getOdor())
                + " ; neuron: "
                + str(neuron.get_id())
                + " ; spikes: "
                + neuron.get_output_spikes(),
                "\n",
            )
