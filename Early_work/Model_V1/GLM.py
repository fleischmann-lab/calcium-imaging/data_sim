import numpy as np
import pandas as pd
import math as m
import matplotlib.pyplot as plt

# from scipy.stats import poisson
from scipy import signal
from Filter import Filter
from Trial import Trial

# import statistics
# from pynwb import NWBFile
from pynwb import NWBHDF5IO

# import os
# from pathlib import Path
# from scipy.signal import savgol_filter
from sklearn import preprocessing


class Neuron:
    def __init__(self, mouse):
        if mouse == 7:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#7.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
        if mouse == 8:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#8.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
        if mouse == 9:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#9.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
        nwbfile_in = io.read()
        # print()
        # Modulation data to use
        self._flow = np.array(nwbfile_in.acquisition["flow"].data)
        # To keep track of odor presentation for each flow/fluo trace
        self._odor = np.array(nwbfile_in.stimulus["odor"].data)
        # self._fluorescence = nwbfile_in.processing["ophys"]["Fluorescence"]
        self._spikes = None  # Spike train from fluo data
        self._output_spikes = []  # To record spike rate/number of spikes
        self._id = 0  # Neuron ID to set when instantiating class
        self._file = nwbfile_in  # For printing of entire NWB file
        self._trial_window = 30000
        # 2 400 000 / 30 000 = 80
        # 2 400 000 / 30 000 = 80
        self._trial_count = int(len(self._flow) / self._trial_window)
        self._spike_window = int(
            len(self._fluorescence[0]) / self._trial_count
        )  # 10 880/80 = 136
        self._indices = None  # Indices of spike within each trial (163 data points)
        self._filter_set = {}  # Dictionary of filters made
        self._trial_set = {}
        self._unnormalized = None  # Flow data without 0 mean
        # plt.plot(self._flow)
        # plt.show()

    def set_id(self, id):
        self._id = id

    def get_id(self):
        return self._id

    def get_NWB(self):
        return self._file

    def get_flow(self):
        return self._flow

    def get_fluorescence(self):
        return self._fluorescence

    # Using window size of 5 points
    def convert_fluorescence_to_spikes(self):
        array = []
        for roi in self._fluorescence:
            arr = []
            avg = np.mean(roi)
            std = np.std(roi)
            c = 0
            window = False
            for i in range(len(roi)):
                if c == 5:
                    c = 0
                    window = False
                # avg and std per roi throughout entire session --> double check
                if (avg + 0 * std) < roi[i]:
                    if window == False:
                        # print('hi')
                        arr.append(1)
                        c = 0
                    if window == True:
                        arr.append(0)
                    window = True
                    c += 1
                else:
                    if c != 0:
                        c += 1
                    arr.append(0)
            array.append(arr)
        self._spikes = np.array(array)
        return self._spikes

    # Downsample to fit sniff data to fluorescence data
    def downsample(self):
        odor_columns = np.arange(0, 2400000, 30000)
        self._odor = self._odor[odor_columns]
        self._flow = signal.resample(self._flow, self._spike_window * self._trial_count)
        print(self._flow)
        return self._flow, self._odor

    # Record index of spikes within each 136 data point window (i.e. each trial)
    def get_spike_indices(self):
        self._indices = np.argwhere(self._spikes > 0)
        return self._indices

    # Normalize flow to have zero mean
    def normalize_flow(self):
        # self._flow = (self._flow - np.mean(self._flow)) / np.std(self._flow)
        # Standardize a dataset along any axis
        # Center to the mean and component wise scale to unit variance.
        self._flow = preprocessing.scale(self._flow)
        # for i in range(len(self._flow)):
        #     # print(self._flow[i])
        #     if self._flow[i] > 6 or self._flow[i] < -6:
        #         self._flow[i] = np.mean(self._flow)
        return self._flow

    # Get STA arrays for all spike trains across first ten trials (makes set of linear filters)
    # 45 to odor on + 136 each time
    def compute_STA(self):
        for spike in self._indices:
            train = spike[0]
            # print(train)
            index = spike[1]
            # print(index)
            flow = self._flow
            filt = np.zeros(self._spike_window)
            if index >= self._spike_window:
                filt = flow[index - self._spike_window : index]
            elif index < self._spike_window and index >= 45:
                filt[self._spike_window - index : index] = 0
            odor_index = index // self._spike_window
            if index % self._spike_window < 45:
                odor = self._odor[odor_index - 1]
            if index % self._spike_window >= 45:
                odor = self._odor[odor_index]
            if len(filt) == self._spike_window:
                # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
                # yhat = savgol_filter(filt, 51, 3)
                filt_instance = Filter(odor, train, filt)  # CHANGE FILT BACK TO YHAT
                self._filter_set[filt_instance] = filt
        # print(self._filter_set)
        return self._filter_set

    # Applies linear filter to perform dimensionality reduction to flow data
    def L_stage(self, trial, filt):
        L = np.dot(trial, filt)
        return L

    def N_stage(self, L):
        lam = float(1 / (1 + m.exp(-(L - 270000) / 10000)))
        return lam

    def gen_spikes(self, lam):
        spikes = np.random.poisson(lam, size=30000)
        spike_time = []
        timestamp = float(0.0)
        num_spikes = 0
        # print()
        for val in spikes:
            # print(val)
            if val > 0:
                num_spikes += 1
                spike_time.append(timestamp)
            timestamp += float(0.001)
        return num_spikes

    def get_trial_set(self):
        for i in range(self._trial_count):
            # print(i)
            trial = self._flow[i * 136 : (i + 1) * 136]
            odor = self._odor[i]
            trial_instance = Trial(odor, trial, id)  # CHANGE FILT BACK TO YHAT
            self._trial_set[trial_instance] = trial
        return self._trial_set

    def get_odor_set(self):
        return self._odor
