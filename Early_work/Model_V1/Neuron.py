# import datetime
# import time
import numpy as np
import pandas as pd
import seaborn as sns

# import random
import matplotlib.pyplot as plot
import math as m

# from scipy.stats import poisson
# from sklearn import preprocessing

# from get_data import Data
# from Trial import Trial
import statistics

# from decimal import Decimal as d
# from pynwb import NWBFile
from pynwb import NWBHDF5IO

# import os
# from pathlib import Path
from scipy.signal import savgol_filter


class Filter:
    def __init__(self, odor, roi, filt):
        self._odor = odor
        self._roi = roi
        self._filter = filt

    def get_odor(self):
        return self._odor

    def get_roi(self):
        return self._roi

    def get_filter(self):
        return self._filter


class Neuron:
    def __init__(self, nmb, nmb2):
        if nmb == 7:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#7.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
            # if nmb2 == 1:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse7Plane1.npy")
            #     print("MOUSE 7 PLANE 1")
            # if nmb2 == 2:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse7Plane2.npy")
            #     print("MOUSE 7 PLANE 2")
            # if nmb2 == 3:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse7Plane3.npy")
            #     print("MOUSE 7 PLANE 3")
            # self._fluorescence = np.concatenate((self._fluorescence1, self._fluorescence2, self._fluorescence3))
        if nmb == 8:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#8.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
            # if nmb2 == 1:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse8Plane1.npy")
            #     print("MOUSE 8 PLANE 1")
            # if nmb2 == 2:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse8Plane2.npy")
            #     print("MOUSE 8 PLANE 2")
            # if nmb2 == 3:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse8Plane3.npy")
            #     print("MOUSE 8 PLANE 3")
            # self._fluorescence = np.concatenate((self._fluorescence1, self._fluorescence2, self._fluorescence3))
            # self._fluorescence = self._fluorescence3
        if nmb == 9:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#9.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
            # if nmb2 == 1:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse9Plane1.npy")
            #     print("MOUSE 9 PLANE 1")
            # if nmb2 == 2:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse9Plane2.npy")
            #     print("MOUSE 9 PLANE 2")
            # if nmb2 == 3:
            #     self._fluorescence = np.load("/Users/andrewaoun/Downloads/Mouse9Plane3.npy")
            #     print("MOUSE 9 PLANE 3")
            # self._fluorescence = np.concatenate((self._fluorescence1, self._fluorescence2, self._fluorescence3))
            # self._fluorescence = self._fluorescence3
        if nmb == 163:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#163.nwb", "r")
        if nmb == 164:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#164.nwb", "r")
        nwbfile_in = io.read()
        self._flow = np.array(
            nwbfile_in.acquisition["flow"].data
        )  # Modulation data to use
        self._trials = np.array(nwbfile_in.intervals["trials"])
        self._odor = np.array(
            nwbfile_in.stimulus["odor"].data
        )  # To keep track of odor presentation for each flow/fluo trace
        # self._fluorescence = nwbfile_in.processing["ophys"]["Fluorescence"]
        self._spikes = None  # Spike train from fluo data
        self._output_spikes = []  # To record spike rate/number of spikes
        self._id = 0  # Neuron ID to set when instantiating class
        self._file = nwbfile_in  # For printing of entire NWB file
        self._trial_window = 30000
        self._trial_count = int(
            len(self._flow) / self._trial_window
        )  # 2 400 000 / 30 000 = 80
        self._spike_window = int(
            len(self._fluorescence[0]) / self._trial_count
        )  # 10 880/80 = 136
        self._indices = None  # Indices of spike within each trial (163 data points)
        self._filter_set = {}  # Dictionary of filters made
        self._unnormalized = None  # Flow data without 0 mean
        self._odor_activation = []  # Keeps track of odors that activate this neuron
        self._on = []
        self._off = []

    def set_id(self, id):
        self._id = id

    def get_id(self):
        return self._id

    def get_NWB(self):
        return self._file

    def get_flow(self):
        return self._flow

    def get_trials(self):
        return self._trials

    def set_odor_activation(self, odor):
        self._odor_activation.append(odor)

    def get_odor_activation(self):
        return self._odor_activation

    def get_fluorescence(self):
        return len(self._fluorescence[0])

    # Threshold fluorescence data to output spike trains
    def convert_fluorescence_to_spikes(self):
        # array = []
        # sum = 0
        # rois = []
        # for roi in self._fluorescence:
        #     for val in roi:
        #         sum += val
        #         rois.append(val)
        # avg = sum/len(rois)
        # std = statistics.stdev(rois)
        # for roi in self._fluorescence:
        #     arr = []
        #     for val in roi:
        #         if (avg + 2.5 * std) < val:
        #             arr.append(1)
        #             self._on.append(val)
        #         else:
        #             arr.append(0)
        #             self._off.append(val)
        #     array.append(arr)
        array = []
        for roi in self._fluorescence:
            arr = []
            sum = 0
            for val in roi:
                sum += val
            avg = sum / len(roi)
            std = statistics.stdev(roi)
            c = 0
            window = False
            for i in range(len(roi)):
                if c == 5:
                    c = 0
                    window = False
                if (avg + 2 * std) < roi[
                    i
                ]:  # avg and std per roi throughout entire session --> double check
                    if window == False:
                        arr.append(1)
                    if window == True:
                        arr.append(0)
                    window = True
                    c += 1
                    # if c == None and window < 5:
                    #     arr.append(1)
                    #     self._on.append(val)
                    # else:
                    #     arr.append(0)
                    # c = roi[i]
                    # window += 1
                else:
                    if c != 0:
                        c += 1
                    arr.append(0)
                    self._off.append(val)
                    # c = None
            array.append(arr)
        # out = []
        # timestamp = float(0.0)
        # for nmb in array[0]:
        #     if nmb > 0:
        #         out.append(timestamp)
        #     timestamp += float(trials*30*2/len(array[0]))
        sum = 0
        for val in self._on:
            sum += val
        # print(len(self._on))
        sum = 0
        for val in self._off:
            sum += val
        # print(len(self._off))
        self._spikes = array
        # timestamp = 0.0
        # spike_time = []
        # for i in range(136):
        #     if array[0][i] != 0:
        #         spike_time.append(timestamp)
        #     timestamp += float(30/10880)
        # plot.eventplot(spike_time)
        # plot.show()
        return array

    # Downsample to fit sniff data to fluorescence data
    def downsample(self):
        fluo_window = int(self._trial_window / self._spike_window)  # 30 000/136 = 220
        array = []
        unnormalized = []
        array_odor = []
        c = self._trial_window
        while c <= len(self._flow):
            temp = self._flow[c - self._trial_window : c]
            temp_odor = self._odor[c - self._trial_window : c]
            odor = temp_odor[0]
            array_odor.append(odor)
            for i in range(self._spike_window):
                sum = 0
                for val in temp[i * fluo_window : (fluo_window * (i + 1))]:
                    sum += val
                # for val in temp_odor[i * fluo_window : (fluo_window * (i + 1))]:
                #     sum_odor = val
                avg = sum / fluo_window
                # avg_odor = sum_odor / fluo_window
                array.append(avg)
                unnormalized.append(avg)
            c += self._trial_window
            # sum_odor = 0
            # for val in temp_odor[i * fluo_window : (fluo_window * (i + 1))]:
            #     sum_odor = val
            # array_odor.append(sum_odor)
            # c += self._trial_window
        self._unnormalized = unnormalized
        self._flow = array
        self._odor = array_odor
        print("hi")
        print(self._odor)
        # plot.plot(self._flow)
        # plot.show()
        # USE DOWNSAMPLING METHOD ANDREA FOUND #####

    # Record index of spikes within each 136 data point window (i.e. each trial)
    def get_spike_indices(self):
        indices = [[] for i in range(len(self._spikes))]
        # indices = []
        j = 0
        for train in self._spikes:
            # c = 0
            # while c < self._trial_count:
            # window = train[c * self._spike_window : (c + 1) * self._spike_window]
            # for i in range(self._spike_window):
            #     if window[i] == 1:
            #         indices[j].append(i)
            #     else:
            #         indices[j].append(None)

            for i in range(len(train)):
                if train[i] == 1:
                    indices[j].append(i)
                else:
                    indices[j].append(None)
                # c += 1
            j += 1
        self._indices = indices
        return indices

    # Normalize flow to have zero mean
    def normalize_flow(self):
        # sum = 0
        # for val in self._flow:
        #     sum += val
        # avg = sum / len(self._flow)
        # for i in range(len(self._flow)):
        #     self._flow[i] = self._flow[i] - avg
        # return self._flow

        # c = 0
        # array = [[] for i in range(self._spike_window)]
        # while c < self._trial_count:
        #     flow = self._flow[c * self._spike_window: (c+1) * self._spike_window]
        #     for i in range(len(flow)):
        #         array[i].append(flow[i])
        #     c += 1
        # mean = []
        # for arr in array:
        #     sum = 0
        #     for val in arr:
        #         sum += val
        #     avg = sum/len(arr)
        #     mean.append(avg)
        # # plot.plot(mean)
        # # plot.show()
        # c = 0
        # new = []
        # while c < self._trial_count:
        #     flow = self._flow[c * self._spike_window: (c+1) * self._spike_window]
        #     for i in range(len(flow)):
        #         new.append(flow[i] - mean[i])
        #     c += 1
        # self._flow = new

        sum = 0
        for val in self._flow:
            sum += val
        avg = sum / len(self._flow)
        for i in range(len(self._flow)):
            self._flow[i] -= avg
        # plot.plot(self._flow)
        # plot.show()
        return self._flow

    # Get STA arrays for all spike trains across first ten trials (makes set of linear filters)
    # 45 to odor on + 136 each time
    def compute_STA(self):
        for j in range(len(self._indices)):
            indices = self._indices[j][0 : (80 * self._spike_window)]
            # spikes = self._spikes[j][0:(10*self._spike_window)]
            flow = self._flow[0 : (80 * self._spike_window)]
            for i in range((80 * self._spike_window)):
                if indices[i] != None:
                    filt = []
                    if i >= self._spike_window:
                        for j in range(i - self._spike_window, i):
                            filt.append(flow[j])
                    elif i < self._spike_window and i >= 45:
                        for j in range(0, self._spike_window - i):
                            filt.append(0)
                        for j in range(self._spike_window - i, i):
                            filt.append(flow[j])
                    odor_index = i // self._spike_window
                    if i % self._spike_window < 45:
                        odor = self._odor[odor_index - 1]
                    if i % self._spike_window >= 45:
                        # print(odor_index)
                        odor = self._odor[odor_index]
                    if len(filt) == self._spike_window:
                        # print(odor)
                        yhat = savgol_filter(
                            filt, 51, 3
                        )  # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
                        filt_instance = Filter(
                            odor, j, yhat
                        )  # CHANGE FILT BACK TO YHAT
                        self._filter_set[filt_instance] = yhat
                    # self._filter_set[filt_instance] = filt

        # c = 0
        # while c < 10:
        #     for j in range(len(self._spikes)):
        #         indices = self._indices[j][
        #             c * self._spike_window : (c + 1) * self._spike_window
        #         ]
        #         filt = [[] for i in range(self._spike_window)]
        #         spikes = self._spikes[j][
        #             c * self._spike_window : (c + 1) * self._spike_window
        #         ]
        #         flow = self._flow[c * self._spike_window : (c + 1) * self._spike_window]
        #         for i in range(len(spikes)):
        #             if spikes[i] == 1:
        #                 index = int(indices[i])
        #                 mid = len(flow) / 2
        #                 diff = int(index - mid)
        #                 if diff < 0:
        #                     lim = int(len(flow) + diff)
        #                     for i in range(0, lim):
        #                         filt[i + abs(diff)].append(flow[i])
        #                 if diff >= 0:
        #                     lim = int(diff)
        #                     for i in range(lim, len(flow)):
        #                         filt[i - abs(diff)].append(flow[i])
        #         temp = []
        #         for arr in filt:
        #             sum = 0
        #             for val in arr:
        #                 sum += val
        #             if len(arr) != 0:
        #                 avg = sum / len(arr)
        #                 temp.append(avg)
        #             else:
        #                 temp.append(0)
        #         filt = temp
        #         to_append = False
        #         for val in filt:
        #             if val != 0:
        #                 to_append = True
        #         if to_append == True:
        #             yhat = savgol_filter(
        #                 filt, 51, 3
        #             )  # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
        #             filt_instance = Filter(self._odor[c], j, yhat) # CHANGE FILT BACK TO YHAT
        #             self._filter_set[filt_instance] = yhat
        #             # self._filter_set[filt_instance] = filt
        #             # plot.title(filt_instance)
        #             # plot.xlabel(self._odor[c])
        #             # plot.ylabel(j)
        #             # plot.plot(yhat)
        #             # plot.show()
        #     c += 1

    # Computes average filter from set of filters
    def get_average_filter(self):
        arr = [[] for i in range(self._spike_window)]
        for filt in self._filter_set:
            k = 0
            for val in filt.get_filter():
                arr[k].append(val)
                k += 1
        arr2 = []
        for vals in arr:
            sum = 0
            for val in vals:
                sum += val
            arr2.append(sum / len(vals))
        yhat = savgol_filter(
            arr2, 51, 3
        )  # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
        plot.title("Average across all odors")
        plot.xlabel("Time bins")
        plot.ylabel("Spike triggered average values")
        # print(yhat)
        plot.plot(yhat)
        # plot.plot(arr2)
        plot.show()
        print("HERE")
        return yhat

    # Computes average filter for each odor
    def get_average_filter_by_odor(self, odor):
        print(len(self._filter_set))
        # for filt in self._filter_set:
        # print(filt.get_odor())
        arr = [[] for i in range(self._spike_window)]
        for filt in self._filter_set:
            if filt.get_odor() == odor:
                k = 0
                for val in filt.get_filter():
                    arr[k].append(val)
                    k += 1
        arr2 = []
        for vals in arr:
            sum = 0
            for val in vals:
                sum += val
            arr2.append(sum / len(vals))
        yhat = savgol_filter(
            arr2, 51, 3
        )  # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
        plot.title(odor)
        plot.xlabel("Time bins")
        plot.ylabel("Spike triggered average values")
        # print(yhat)
        plot.plot(yhat)
        # plot.plot(arr2)
        plot.show()
        return yhat

    # Correlation matrix for entire filter set/averaged filters
    def avg_filt_corr_matrix(self, dat):
        data = {
            "164": dat.pop(),
            "163": dat.pop(),
            "9": dat.pop(),
            "8": dat.pop(),
            "7": dat.pop(),
        }
        # data = self._filter_set
        df = pd.DataFrame.from_dict(data)
        # print(df)
        # df = pd.DataFrame(data,columns=['10','9','8','7','6','5','4','3','2','1'])
        corr = df.corr()
        sns.heatmap(corr, annot=True)
        # ax = sns.heatmap(
        #     corr,
        #     vmin=-1,
        #     vmax=1,
        #     center=0,
        #     cmap=sns.diverging_palette(20, 220, n=200),
        #     square=True,
        # )
        # ax.set_xticklabels(
        #     ax.get_xticklabels(), rotation=45, horizontalalignment="right"
        # )
        plot.show()

    # Applies linear filter to perform dimensionality reduction to flow data --> CAN SELECT FILTER TO USE + ODOR PRESENTED
    def L_stage(self, filt, odor):
        # print(self._unnormalized)
        # c = random.randint(10,self._trial_count)
        # while self._odor[c] != odor:
        #     c = random.randint(10,self._trial_count)
        c = 10
        out = []
        while c < 80:
            flow = self._unnormalized[
                c * self._spike_window : (c + 1) * self._spike_window
            ]
            sum = 0
            for i in range(self._spike_window):
                val = float(flow[i] * filt[i])
                sum += val
            out.append(sum)
            c += 1
        # print(out)
        return out

    def N_stage(self, vals):
        lam = 0
        a = []
        for val in vals:
            if val != 0:
                # print(val)
                # print(m.exp(-(val-270000)/10000))
                lam = float(1 / (1 + m.exp(-(val - 270000) / 10000)))
            # print(lam)
            a.append(lam)
        # print(a)
        return a

    def gen_spikes(self, lam):
        spikes = {}
        c = 0
        for l in lam:
            # self._output_spikes.append(np.random.poisson(l, size=self._spike_window))
            self._output_spikes.append(np.random.poisson(l, size=30000))
            spike_time = []
            timestamp = float(0.0)
            # print(self._output_spikes)
            s = 0
            for val in self._output_spikes[c]:
                if val > 0:
                    s += 1
                    spike_time.append(timestamp)
                # timestamp += float(self._spike_window/30)
                timestamp += float(0.001)
            spikes[self._odor[c]] = s
            c += 1
        # plot.eventplot(spikes)
        # plot.show()
        # plot.eventplot(spikes[0])
        # plot.show()
        # print(self._output_spikes[0])
        # print(s)
        print(spikes)
        return spikes


neuron_set = []
neuron7_1 = Neuron(7, 1)
# neuron7_2 = Neuron(7, 2)
# neuron7_3 = Neuron(7, 3)
neuron8_1 = Neuron(8, 1)
# neuron8_2 = Neuron(8, 2)
# neuron8_3 = Neuron(8, 3)
neuron9_1 = Neuron(9, 1)
# neuron9_2 = Neuron(9, 2)
# neuron9_3 = Neuron(9, 3)
neuron_set.append(neuron7_1)
# neuron_set.append(neuron7_2)
# neuron_set.append(neuron7_3)
neuron_set.append(neuron8_1)
# neuron_set.append(neuron8_2)
# neuron_set.append(neuron8_3)
neuron_set.append(neuron9_1)
# neuron_set.append(neuron9_2)
# neuron_set.append(neuron9_3)
# neuron163 = Neuron(163)
# neuron164 = Neuron(164)
# print(neuron.get_NWB())

avg = [
    3.69958864,
    3.73822062,
    3.77160681,
    3.80006701,
    3.82392102,
    3.84348861,
    3.85908959,
    3.87104374,
    3.87967086,
    3.88529074,
    3.88822316,
    3.88878792,
    3.88730481,
    3.88409361,
    3.87947413,
    3.87376615,
    3.86728947,
    3.86036387,
    3.85330914,
    3.84644508,
    3.84009148,
    3.83456812,
    3.83019481,
    3.82729132,
    3.82617745,
    3.827173,
    3.83410659,
    3.84505171,
    3.86032657,
    3.88020765,
    3.90493755,
    3.93473107,
    3.96976636,
    4.01017535,
    4.05603635,
    4.1073748,
    3.90493755,
    3.93473107,
    3.96976636,
    4.01017535,
    4.05603635,
    4.1073748,
    3.90493755,
    3.93473107,
    3.96976636,
    4.01017535,
    4.05603635,
    4.1073748,
    3.90493755,
    3.93473107,
    3.96976636,
    4.01017535,
    4.05603635,
    4.1073748,
    4.16414415,
    4.22623994,
    4.29349314,
    4.3656808,
    4.44253099,
    4.52372803,
    4.60889323,
    4.69758006,
    4.78927588,
    4.88337382,
    4.97920166,
    5.07602726,
    5.17308073,
    5.26955478,
    5.36461917,
    5.45744434,
    5.54722238,
    5.63316587,
    5.71451108,
    5.79052848,
    5.86053734,
    5.92388432,
    5.97997718,
    6.02830555,
    6.06843095,
    6.09998546,
    6.12267581,
    6.13631029,
    6.14077028,
    6.13603139,
    6.12214843,
    6.09926251,
    6.06758261,
    6.02738519,
    5.9790131,
    5.92288292,
    5.85945859,
    5.78922497,
    5.71270191,
    5.63044547,
    5.54303227,
    5.45105927,
    5.35513378,
    5.25591494,
    5.15407934,
    5.05030458,
    4.94524334,
    4.839518,
    4.73372422,
    4.62842876,
    4.52423801,
    4.42161172,
    4.32096387,
    4.22266622,
    4.1270345,
    4.03431768,
    3.94468515,
    3.85824148,
    3.77503275,
    3.69504908,
    3.61826542,
    3.54462963,
    3.47407181,
    3.40649567,
    3.34179098,
    3.27983304,
    3.22046878,
    3.16354544,
    3.10890076,
    3.0563665,
    3.00577667,
    2.95696237,
    2.90977665,
    2.86408216,
    2.81974165,
    2.77705993,
    2.73557289,
    2.69518817,
    2.65581341,
    2.61735624,
    2.57972432,
    2.54282527,
    2.50656673,
    2.47085636,
    2.43560177,
    2.40071063,
    2.36609056,
    2.3316492,
    2.2972942,
    2.26293318,
    2.22847381,
    2.1938237,
    2.15889051,
    2.12358187,
    2.08780542,
    2.0514688,
    2.01447965,
    1.97674561,
    1.93817432,
    1.89867341,
]


for neuron in neuron_set:
    print("NEW")
    neuron.convert_fluorescence_to_spikes()
    neuron.downsample()
    neuron.get_spike_indices()
    neuron.normalize_flow()
    neuron.compute_STA()
    # filt = neuron.get_average_filter()
    out = neuron.L_stage(avg, 2)
    lam = neuron.N_stage(out)
    neuron.gen_spikes(lam)

# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(1)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(2)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(3)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(4)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(5)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(6)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(7)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(8)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(9)
# for neuron in neuron_set:
#     filt = neuron.get_average_filter_by_odor(10)
# out = neuron.L_stage(avg, 2)
# print(out)
# lam = neuron.N_stage(sum)
# print(lam)
# neuron.gen_spikes(lam)

# arr = [[] for i in range(136)]
# for filt in avgs:
#     k = 0
#     for val in filt:
#         arr[k].append(val)
#         k += 1
# arr2 = []
# for vals in arr:
#     sum = 0
#     for val in vals:
#         sum += val
#     arr2.append(sum / len(vals))
# yhat = savgol_filter(arr2, 51, 3)  # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
# print(yhat)
# print(len(yhat))
# plot.title('Average across all odors')
# plot.xlabel("Time bins")
# plot.ylabel("Spike triggered average values")
# # print(yhat)
# plot.plot(yhat)
# # plot.plot(arr2)
# plot.show()


# neuron8.convert_fluorescence_to_spikes()
# neuron8.downsample()
# neuron8.get_spike_indices()
# neuron8.normalize_flow()
# neuron8.compute_STA()
# neuron9.convert_fluorescence_to_spikes()
# neuron9.downsample()
# neuron9.get_spike_indices()
# neuron9.normalize_flow()
# neuron9.compute_STA()
# neuron163.convert_fluorescence_to_spikes()
# neuron163.downsample()
# neuron163.get_spike_indices()
# neuron163.normalize_flow()
# neuron163.compute_STA()
# neuron164.convert_fluorescence_to_spikes()
# neuron164.downsample()
# neuron164.get_spike_indices()
# neuron164.normalize_flow()
# neuron164.compute_STA()

# arr = []
# arr.append(neuron7.get_average_filter())
# arr.append(neuron8.get_average_filter())
# arr.append(neuron9.get_average_filter())
# arr.append(neuron163.get_average_filter())
# arr.append(neuron164.get_average_filter())
# neuron7.avg_filt_corr_matrix(arr)

# arr = []
# arr.append(neuron.get_average_filter_by_odor(1))
# arr.append(neuron.get_average_filter_by_odor(2))
# arr.append(neuron.get_average_filter_by_odor(3))
# arr.append(neuron.get_average_filter_by_odor(4))
# arr.append(neuron.get_average_filter_by_odor(5))
# arr.append(neuron.get_average_filter_by_odor(6))
# arr.append(neuron.get_average_filter_by_odor(7))
# arr.append(neuron.get_average_filter_by_odor(8))
# arr.append(neuron.get_average_filter_by_odor(9))
# arr.append(neuron.get_average_filter_by_odor(10))

# neuron.avg_filt_corr_matrix(arr)

# filt = neuron.get_average_filter()
# array = neuron.L_stage(filt, 2)

# sum = 0
# for val in array:
#     sum += val
# avg = sum/len(array)
# print(avg)
# print(max(array))
# print(min(array))
# print(max(array)-min(array))


# sum = neuron.L_stage(filt, 2)
# lam = neuron.N_stage(sum)
# neuron.gen_spikes(lam)

# neuron.get_average_filter_by_odor(1)
# neuron.get_average_filter_by_odor(2)
# neuron.get_average_filter_by_odor(3)
# neuron.get_average_filter_by_odor(4)
# neuron.get_average_filter_by_odor(5)
# neuron.get_average_filter_by_odor(6)
# neuron.get_average_filter_by_odor(7)
# neuron.get_average_filter_by_odor(8)
# neuron.get_average_filter_by_odor(9)
# neuron.get_average_filter_by_odor(10)

avg = [
    3.69958864,
    3.73822062,
    3.77160681,
    3.80006701,
    3.82392102,
    3.84348861,
    3.85908959,
    3.87104374,
    3.87967086,
    3.88529074,
    3.88822316,
    3.88878792,
    3.88730481,
    3.88409361,
    3.87947413,
    3.87376615,
    3.86728947,
    3.86036387,
    3.85330914,
    3.84644508,
    3.84009148,
    3.83456812,
    3.83019481,
    3.82729132,
    3.82617745,
    3.827173,
    3.83410659,
    3.84505171,
    3.86032657,
    3.88020765,
    3.90493755,
    3.93473107,
    3.96976636,
    4.01017535,
    4.05603635,
    4.1073748,
    3.90493755,
    3.93473107,
    3.96976636,
    4.01017535,
    4.05603635,
    4.1073748,
    3.90493755,
    3.93473107,
    3.96976636,
    4.01017535,
    4.05603635,
    4.1073748,
    3.90493755,
    3.93473107,
    3.96976636,
    4.01017535,
    4.05603635,
    4.1073748,
    4.16414415,
    4.22623994,
    4.29349314,
    4.3656808,
    4.44253099,
    4.52372803,
    4.60889323,
    4.69758006,
    4.78927588,
    4.88337382,
    4.97920166,
    5.07602726,
    5.17308073,
    5.26955478,
    5.36461917,
    5.45744434,
    5.54722238,
    5.63316587,
    5.71451108,
    5.79052848,
    5.86053734,
    5.92388432,
    5.97997718,
    6.02830555,
    6.06843095,
    6.09998546,
    6.12267581,
    6.13631029,
    6.14077028,
    6.13603139,
    6.12214843,
    6.09926251,
    6.06758261,
    6.02738519,
    5.9790131,
    5.92288292,
    5.85945859,
    5.78922497,
    5.71270191,
    5.63044547,
    5.54303227,
    5.45105927,
    5.35513378,
    5.25591494,
    5.15407934,
    5.05030458,
    4.94524334,
    4.839518,
    4.73372422,
    4.62842876,
    4.52423801,
    4.42161172,
    4.32096387,
    4.22266622,
    4.1270345,
    4.03431768,
    3.94468515,
    3.85824148,
    3.77503275,
    3.69504908,
    3.61826542,
    3.54462963,
    3.47407181,
    3.40649567,
    3.34179098,
    3.27983304,
    3.22046878,
    3.16354544,
    3.10890076,
    3.0563665,
    3.00577667,
    2.95696237,
    2.90977665,
    2.86408216,
    2.81974165,
    2.77705993,
    2.73557289,
    2.69518817,
    2.65581341,
    2.61735624,
    2.57972432,
    2.54282527,
    2.50656673,
    2.47085636,
    2.43560177,
    2.40071063,
    2.36609056,
    2.3316492,
    2.2972942,
    2.26293318,
    2.22847381,
    2.1938237,
    2.15889051,
    2.12358187,
    2.08780542,
    2.0514688,
    2.01447965,
    1.97674561,
    1.93817432,
    1.89867341,
]
