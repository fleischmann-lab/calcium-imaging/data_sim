# from scipy.signal import savgol_filter
import matplotlib.pyplot as plot
import pandas as pd
import seaborn as sns
import numpy as np

# Computes average filter from set of filters


def avg_all(filter_set):
    arr = []
    for filt in filter_set:
        arr.append(filt.get_filter())
    # print(arr)
    avg = np.mean(arr, axis=0)
    # yhat = savgol_filter(avg, 51, 3)  # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
    plot.title("Average across all odors")
    plot.xlabel("Time")
    plot.ylabel("Flow")
    plot.plot(avg)
    plot.show()
    return avg


# Computes average filter for each odor


def avg_odor(filter_set, odor):
    arr = []
    for filt in filter_set:
        if filt.get_odor() == odor:
            arr.append(filt.get_filter())
    # print(arr)
    avg = np.mean(arr, axis=0)
    # yhat = savgol_filter(avg, 51, 3)  # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
    # plot.title("Average for odor " + str(odor))
    # plot.xlabel("Time")
    # plot.ylabel("Flow")
    # plot.plot(avg)
    # plot.show()
    return avg


# Correlation matrix for entire filter set/averaged filters


def avg_filt_corr_matrix(dat):
    data = {
        "164": dat.pop(),
        "163": dat.pop(),
        "9": dat.pop(),
        "8": dat.pop(),
        "7": dat.pop(),
    }
    df = pd.DataFrame.from_dict(data)
    corr = df.corr()
    sns.heatmap(corr, annot=True)
    ax = sns.heatmap(
        corr,
        vmin=-1,
        vmax=1,
        center=0,
        cmap=sns.diverging_palette(20, 220, n=200),
        square=True,
    )
    ax.set_xticklabels(ax.get_xticklabels(), rotation=45, horizontalalignment="right")
    plot.show()
