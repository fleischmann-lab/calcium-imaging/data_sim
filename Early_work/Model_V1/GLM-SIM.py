import datetime
import time
import numpy as np
import pandas as pd
import seaborn as sns
import random
import matplotlib.pyplot as plot
import math as m
from scipy.stats import poisson
from sklearn.preprocessing import normalize
from get_data import Data
from Trial import Trial
import statistics
from decimal import Decimal as d
from pynwb import NWBFile
from pynwb import NWBHDF5IO
import os
from pathlib import Path
from scipy.signal import savgol_filter


class Filter:
    def __init__(self, odor, roi, filt):
        self._odor = odor
        self._roi = roi
        self._filter = filt

    def get_odor(self):
        return self._odor

    def get_roi(self):
        return self._roi

    def get_filter(self):
        return self._filter


class Neuron:
    def __init__(self, nmb, nmb2):
        if nmb == 7:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#7.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse7Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
        if nmb == 8:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#8.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse8Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
        if nmb == 9:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#9.nwb", "r")
            self._fluorescence1 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane1.npy"
            )
            self._fluorescence2 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane2.npy"
            )
            self._fluorescence3 = np.load(
                "/Users/andrewaoun/Downloads/Mouse9Plane3.npy"
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence1, self._fluorescence2), axis=0
            )
            self._fluorescence = np.concatenate(
                (self._fluorescence, self._fluorescence3), axis=0
            )
        if nmb == 163:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#163.nwb", "r")
        if nmb == 164:
            io = NWBHDF5IO("/Users/andrewaoun/Downloads/Mouse#164.nwb", "r")
        nwbfile_in = io.read()
        self._flow = np.array(
            nwbfile_in.acquisition["flow"].data
        )  # Modulation data to use
        self._trials = np.array(nwbfile_in.intervals["trials"])
        self._odor = np.array(
            nwbfile_in.stimulus["odor"].data
        )  # To keep track of odor presentation for each flow/fluo trace
        # self._fluorescence = nwbfile_in.processing["ophys"]["Fluorescence"]
        self._spikes = None  # Spike train from fluo data
        self._output_spikes = []  # To record spike rate/number of spikes
        self._id = 0  # Neuron ID to set when instantiating class
        self._file = nwbfile_in  # For printing of entire NWB file
        self._trial_window = 30000
        self._trial_count = int(
            len(self._flow) / self._trial_window
        )  # 2 400 000 / 30 000 = 80
        self._spike_window = int(
            len(self._fluorescence[0]) / self._trial_count
        )  # 10 880/80 = 136
        self._indices = None  # Indices of spike within each trial (163 data points)
        self._filter_set = {}  # Dictionary of filters made
        self._unnormalized = None  # Flow data without 0 mean

    def set_id(self, id):
        self._id = id

    def get_id(self):
        return self._id

    def get_NWB(self):
        return self._file

    def get_flow(self):
        return self._flow

    def get_trials(self):
        return self._trials

    def convert_fluorescence_to_spikes(self):
        array = []
        for roi in self._fluorescence:
            arr = []
            sum = 0
            for val in roi:
                sum += val
            avg = sum / len(roi)
            std = statistics.stdev(roi)
            c = 0
            window = False
            for i in range(len(roi)):
                if c == 5:
                    c = 0
                    window = False
                # avg and std per roi throughout entire session --> double check
                if (avg + 2 * std) < roi[i]:
                    if window is False:
                        arr.append(1)
                    if window is not True:
                        arr.append(0)
                    window = True
                    c += 1
                else:
                    if c != 0:
                        c += 1
                    arr.append(0)
                    self._off.append(val)
            array.append(arr)
        sum = 0
        for val in self._on:
            sum += val
        sum = 0
        for val in self._off:
            sum += val
        self._spikes = array
        return array

        # Downsample to fit sniff data to fluorescence data

    def downsample(self):
        fluo_window = int(self._trial_window / self._spike_window)  # 30 000/136 = 220
        array = []
        unnormalized = []
        array_odor = []
        c = self._trial_window
        while c <= len(self._flow):
            temp = self._flow[c - self._trial_window : c]
            temp_odor = self._odor[c - self._trial_window : c]
            odor = temp_odor[0]
            array_odor.append(odor)
            for i in range(self._spike_window):
                sum = 0
                for val in temp[i * fluo_window : (fluo_window * (i + 1))]:
                    sum += val
                # for val in temp_odor[i * fluo_window : (fluo_window * (i + 1))]:
                #     sum_odor = val
                avg = sum / fluo_window
                # avg_odor = sum_odor / fluo_window
                array.append(avg)
                unnormalized.append(avg)
            c += self._trial_window
            # sum_odor = 0
            # for val in temp_odor[i * fluo_window : (fluo_window * (i + 1))]:
            #     sum_odor = val
            # array_odor.append(sum_odor)
            # c += self._trial_window
        self._unnormalized = unnormalized
        self._flow = array
        self._odor = array_odor
        print("hi")
        print(self._odor)
        # plot.plot(self._flow)
        # plot.show()
        # USE DOWNSAMPLING METHOD ANDREA FOUND #####

    # Record index of spikes within each 136 data point window (i.e. each trial)
    def get_spike_indices(self):
        indices = [[] for i in range(len(self._spikes))]
        j = 0
        for train in self._spikes:
            for i in range(len(train)):
                if train[i] == 1:
                    indices[j].append(i)
                else:
                    indices[j].append(None)
            j += 1
        self._indices = indices
        return indices

    # Normalize flow to have zero mean
    def normalize_flow(self):
        sum = 0
        for val in self._flow:
            sum += val
        avg = sum / len(self._flow)
        for i in range(len(self._flow)):
            self._flow[i] -= avg
        return self._flow

    # Get STA arrays for all spike trains across first ten trials (makes set of linear filters)
    # 45 to odor on + 136 each time
    def compute_STA(self):
        for j in range(len(self._indices)):
            indices = self._indices[j][0 : (80 * self._spike_window)]
            flow = self._flow[0 : (80 * self._spike_window)]
            for i in range((80 * self._spike_window)):
                if indices[i] != None:
                    filt = []
                    if i >= self._spike_window:
                        for j in range(i - self._spike_window, i):
                            filt.append(flow[j])
                    elif i < self._spike_window and i >= 45:
                        for j in range(0, self._spike_window - i):
                            filt.append(0)
                        for j in range(self._spike_window - i, i):
                            filt.append(flow[j])
                    odor_index = i // self._spike_window
                    if i % self._spike_window < 45:
                        odor = self._odor[odor_index - 1]
                    if i % self._spike_window >= 45:
                        odor = self._odor[odor_index]
                    if len(filt) == self._spike_window:
                        yhat = savgol_filter(
                            filt, 51, 3
                        )  # window size 51, polynomial order 3 --> SMOOTHES FILTERS SO LESS NOISY, MAYBE WANT NOISE?
                        filt_instance = Filter(
                            odor, j, yhat
                        )  # CHANGE FILT BACK TO YHAT
                        self._filter_set[filt_instance] = yhat
