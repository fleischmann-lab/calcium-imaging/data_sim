import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plot
from scipy.stats import poisson
from scipy import signal
from Filter import Filter
import statistics
from pynwb import NWBFile
from pynwb import NWBHDF5IO
import os
from pathlib import Path
from scipy.signal import savgol_filter
from GLM import Neuron
from filteranalysis import avg_all, avg_odor

if __name__ == "__main__":

    # Generate filter set from which to compute STA
    GLM = Neuron(8)
    print(1)
    GLM.convert_fluorescence_to_spikes()
    print(2)
    GLM.downsample()
    print(3)
    print(4)
    GLM.get_spike_indices()
    print(5)
    GLM.normalize_flow()
    filter_set = GLM.compute_STA()
    print(6)

    # Vizualize STA
    avg = avg_all(filter_set)
    odor1 = avg_odor(filter_set, 1)
    odor2 = avg_odor(filter_set, 2)
    odor3 = avg_odor(filter_set, 3)
    odor4 = avg_odor(filter_set, 4)
    odor5 = avg_odor(filter_set, 5)
    odor6 = avg_odor(filter_set, 6)
    odor7 = avg_odor(filter_set, 7)
    odor8 = avg_odor(filter_set, 8)
    odor9 = avg_odor(filter_set, 9)
    odor10 = avg_odor(filter_set, 10)
