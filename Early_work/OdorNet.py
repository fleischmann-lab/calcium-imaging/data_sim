import datetime
import time
import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plot
import math as m
from scipy.stats import poisson
from sklearn.preprocessing import normalize

# acts as an edge


class Similarity:
    def __init__(self, min, max):
        self._range = [min, max]
        self._odor_pair = []

    def addOdors(self, odor1, odor2):
        self._odor_pair.append(odor1)
        self._odor_pair.append(odor2)

    def getOdorPair(self):
        return self._odor_pair

    def getRange(self):
        return self._range


# acts as a vertex


class Odor:
    def __init__(self, odor):
        self._name = odor
        self._neighbors = []
        self._similarity = []

    def addSimilarity(self, odor, similarity):
        self._neighbors.append(odor)
        self._similarity.append(similarity)

    def getOdor(self):
        return self._name

    def getNeighbors(self):
        return self._neighbors

    def getSimilarity(self):
        return self._similarity


class OdorNet:
    def __init__(self):
        self._odors = []
        self._similairites = []

    def addOdor(self, odor):
        self._odors.append(odor)

    def addOverlap(self, odor1, odor2, similarity):
        # to_overlap = []
        # for odor in self._odors:
        #     print(str(odor.getOdor()))
        #     if odor.getOdor() == str(odor1) or odor.getOdor() == str(odor2):
        #         print(2)
        #         to_overlap.append(odor)
        odor1.addSimilarity(odor2, similarity)
        odor2.addSimilarity(odor1, similarity)
        self._similairites.append(similarity)
        similarity.addOdors(odor1, odor2)

    def getOdorSet(self):
        return self._odors

    def getSimilaritySet(self):
        return self._similairites

    # def getOdorPairs(self, name):
    #     for odor in self._odors:
    #         if odor.getOdor() == name:
    #             to_get = name
    #     to_get


class Ensemble:
    def __init__(self, odor):
        self._odor = odor

    def getOdor(self, odor):
        return self._odor
